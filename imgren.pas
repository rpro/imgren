program imgren;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, CustApp, Crt
  { you can add units after this };

type

  { ImageRename }

  ImageRename = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;

    var gFileExtension: string;    // Global extension
    var iMaxLetterCount: integer;  // The max number countable by letter
    var gAction: string;           // Global action to take.

   end;



{ ImageRename }


procedure ImageRename.DoRun;
var
  ErrorMsg: String;
  i: integer;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('h', 'help');
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;


  if ParamCount = 0 then WriteHelp;

  if ParamCount > 0 then begin
   for i := 1 to ParamCount do begin

       case LowerCase(ParamStr(i)) of
       'help', '?', '/?', '/help' : begin
           WriteHelp;
           end;
       end;

       case copy(LowerCase(ParamStr(i)), 0, 4) of

         // Get the file extension - required
       'ext=' : begin
                  if AnsiPos('.', ParamStr(i) ) > 0 then begin
                   TextColor(Red);
                   writeln('Please enter file extension without a .');
                   WriteHelp;
                   end;
                  gFileExtension := '.' + copy(LowerCase(ParamStr(i)), 5, length(ParamStr(i)));

           end;

         // Get the max letter counter  - not required default 99999
       'max=' : begin
                     try
                     iMaxLetterCount := StrToInt(copy(LowerCase(ParamStr(i)), 5, length(ParamStr(i))));
                     except
                         on E : Exception do  begin
                           TextColor(Red);
                           writeln('Invalid max number.');
                           WriteHelp;
                         end;
                     end;
              end;

         // Simulate (just print output), copy or rename?? - not required default simulate
       'act=' : begin
             if  copy(LowerCase(ParamStr(i)), 5, 8) = 'cpy' then begin
                   gAction := 'cpy';
             end else if copy(LowerCase(ParamStr(i)), 5, 8) = 'ren' then begin
                   gAction := 'ren';
             end else if copy(LowerCase(ParamStr(i)), 5, 8) = 'sim' then begin
                   gAction := 'sim';
             end else if copy(LowerCase(ParamStr(i)), 5, 8) = 'rln' then begin
                   gAction := 'rln';
             end else begin
                 TextColor(Red);
                 writeln ('Invalid action.');
                 WriteHelp();
                 end;
           end;

          else
            // Invalid argument entered
            TextColor(Red);
            writeln('Invalid argument.');
            WriteHelp();
       end;

   end;


   if gAction = '' then gAction := 'sim';
   if iMaxLetterCount = 0 then iMaxLetterCount := 99999;
   if gFileExtension = '' then begin
    TextColor(Red);
    writeln('File extension required.');
    WriteHelp();
   end;





  { add your program here }
  end;



  // stop program loop
  Terminate;
end;

constructor ImageRename.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;

end;

destructor ImageRename.Destroy;
begin
  inherited Destroy;
end;



procedure ImageRename.WriteHelp;
begin
  { add your help code here }
  TextColor(White);
  writeln('');
  writeln('imgren.exe [ext=value] [max=value] [act=value]');
  writeln('');
  writeln('Example:  imgren.exe ext=.jpg max=1000 act=ren');
  writeln('');
  writeln('help - Show this menu.');
  writeln('');
  writeln('ext - The file extension to filter the current working directory on. Required field no default.');
  writeln('');
  writeln('max - The max index count per alpha segment when renaming ie if 999 A999 - Z999.  Defaults to 99999');
  writeln('');
  writeln('act - Action to take either sim, cpy, ren, or rln.  Defaults to sim.');
  writeln('act=sim - Simulates the output of ren. No changes made just a list of filenames.');
  writeln('act=cpy - Copy the files leaving the original file in place.');
  writeln('act=ren - Rename the files which removes the original.');
  writeln('act=rln - Removes leading numbers based on underscore. Example A001_FILE.JPG would become FILE.JPG');
  Terminate;
  Halt;
  Exit;
end;


var
  Application: ImageRename;
  sIn: string;
  CurrentDirFiles: TStringList;
  cAlphaIndex: Char;
  iAlphaIndex: integer;
  iNumericIndex: integer;
  iFileIndex: integer;
  sFileName: string;
  sFileExtention: string;
  FileList : TStringList;
  sNewFilename : string;
  sTmp: string;
  Info : TSearchRec;
  index: integer;
  SourceF, DestF: TFileStream;

begin
  Application:=ImageRename.Create(nil);
  Application.Title:='ImageRename';
  Application.Run;

  FileList := TStringList.Create;

  CurrentDirFiles := TStringList.Create;


  If FindFirst ('*',faAnyFile and faDirectory,Info)=0 then
  begin
    repeat

     // As long as its not a directory
      If (Info.Attr and faDirectory) <> faDirectory then
      begin

        // Does the file exention match
        if lowercase(ExtractFileExt(Info.Name)) = Application.gFileExtension then begin
           FileList.Add(Info.Name);
           TextColor(Green);
           writeln('Found: ' + Info.Name);
        end;

      end;

    until FindNext(info)<>0;

  end;


  if FileList.Count > 26 * Application.iMaxLetterCount then begin
     TextColor(Red);
     writeln('There are more files than this application can rename please increase the max count.');
     Application.Terminate(0);
  end;

  iAlphaIndex := 65;
  iFileIndex := 1;

  for iFileIndex := 1 to FileList.Count do begin

    if iFileIndex <= ( ((iAlphaIndex - 64) * Application.iMaxLetterCount)) then begin

     sNewFilename := char(iAlphaIndex) ;


             if iFileIndex >= Application.iMaxLetterCount
             then sTmp := IntToStr(iFileIndex- ((iAlphaIndex - 64)* Application.iMaxLetterCount) + Application.iMaxLetterCount)
             else sTmp := IntToStr(iFileIndex);

             // Prefix the files with 0's
             while length(sTmp) < length(IntToStr(Application.iMaxLetterCount)) do begin
               sTmp := '0' + sTmp;
             end;

             sNewFilename := sNewFilename + sTmp + '_' + FileList[iFileIndex - 1];
             TextColor(White);
             case Application.gAction of
              'cpy' : begin
                         try
                              writeln ('Copying:' + FileList[iFileIndex - 1] + ' to:  ' + sNewFilename);
                              SourceF := TFileStream.Create( FileList[iFileIndex - 1], fmOpenRead);
                              DestF := TFileStream.Create(sNewFilename, fmCreate);
                              DestF.CopyFrom(SourceF, SourceF.Size);
                              SourceF.Free;
                              DestF.Free;
                              TextColor(Green);
                              writeln(sNewFilename);
                         except
                             on E : Exception do begin
                               TextColor(Red);
                               writeln(E.ClassName+' error raised, with message : '+E.Message);
                             end;
                         end;
                      end;


              'ren' : begin
                      writeln ('Renaming:' + FileList[iFileIndex - 1] + ' to:  ' + sNewFilename);

                       try
                         SourceF := TFileStream.Create( FileList[iFileIndex - 1], fmOpenRead);
                         DestF := TFileStream.Create(sNewFilename, fmCreate);
                         DestF.CopyFrom(SourceF, SourceF.Size);
                         SourceF.Free;
                         DestF.Free;
                         DeleteFile(FileList[iFileIndex - 1]);
                         writeln(sNewFilename);
                       except
                           on E : Exception do begin
                             TextColor(Red);
                             writeln(E.ClassName+' error raised, with message : '+E.Message);
                           end;
                       end;

                      end;

              'sim' : begin
                      writeln ('Simulate:  ' + FileList[iFileIndex - 1] + '   would become:   ' + sNewFilename);
                      end;

              'rln' : begin
                 if AnsiPos('_', FileList[iFileIndex - 1]) > 0 then begin
                  writeln ('Removing leading number from:' + FileList[iFileIndex - 1]);
                  try
                   SourceF := TFileStream.Create( FileList[iFileIndex - 1], fmOpenRead);
                   DestF := TFileStream.Create(copy(FileList[iFileIndex - 1], AnsiPos('_', FileList[iFileIndex - 1]) + 1, length(FileList[iFileIndex - 1]) -  AnsiPos('_', FileList[iFileIndex - 1]) +1), fmCreate);
                   DestF.CopyFrom(SourceF, SourceF.Size);
                   SourceF.Free;
                   DestF.Free;
                   DeleteFile(FileList[iFileIndex - 1]);
                   writeln(copy(FileList[iFileIndex - 1], AnsiPos('_', FileList[iFileIndex - 1]) + 1, length(FileList[iFileIndex - 1]) -  AnsiPos('_', FileList[iFileIndex - 1]) +1));
                   except
                      on E : Exception do  begin
                        TextColor(Red);
                        writeln(E.ClassName+' error raised, with message : '+E.Message);
                      end;
                  end;
                 end;

             end;


             end;
             // Exit the loop if we reached the end of the files
             if iFileIndex = Filelist.Count then break;

             // Reset the file counter to 0 if we have hit the max letter count
//             if iFileIndex = Application.iMaxLetterCount then iAlphaIndex := iAlphaIndex + 1;
             if iFileIndex =  ((iAlphaIndex - 64) * Application.iMaxLetterCount) then iAlphaIndex := iAlphaIndex + 1;

             // Exit the loop we have tried to go over the max allowed values
             if iAlphaIndex > 90 then break;



    end else begin

    end;



  end;

  Application.Free;
end.


